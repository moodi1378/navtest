package com.example.navtest;

import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import io.flutter.facade.Flutter;
import io.flutter.facade.FlutterFragment;

public class Base extends AppCompatActivity {

    FrameLayout viewById;
    View rootview;
    FlutterFragment route1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        viewById = findViewById(R.id.contatinet);

        FullDrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        if (route1 == null) {
            route1 = Flutter.createFragment("route1");
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.nav_view, route1);
            tx.commit();
        }


    }


    public void setRootview(View rootview) {
        this.rootview = rootview;
        if (viewById.getChildCount() != 0) {
            viewById.removeAllViews();
        } else {
            viewById.addView(rootview);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (rootview == null) {

        }
    }
}

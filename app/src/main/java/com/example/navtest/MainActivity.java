package com.example.navtest;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ReplacementSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends Base {

    String test;
    private TextView textView1;
    private Button button;
    private Button button1;
    private Button button2;
    private EditText editText;

    int pos = 0;
    private ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View inflate = LayoutInflater.from(getBaseContext()).inflate(R.layout.activity_main, (ViewGroup) rootview);
        View viewById = inflate.findViewById(R.id.mainactivityrootview);
        setRootview(viewById);

        textView1 = findViewById(R.id.textviews);
        scrollView = findViewById(R.id.scrllview);
//        textView1.setMovementMethod(new ScrollingMovementMethod());
        button = findViewById(R.id.search);
        button1 = findViewById(R.id.back);
        button2 = findViewById(R.id.next);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos < RoundedBackgroundSpan.ys.size()) {
                    scrollView.scrollTo(0, RoundedBackgroundSpan.ys.get(pos) - 45);
                    pos++;
                }
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos--;
                if (pos < 0)
                    pos = 0;
                scrollView.scrollTo(0, RoundedBackgroundSpan.ys.get(pos) - 45);

            }
        });
        editText = findViewById(R.id.edittext);
        String stringToAppend = "hello world";

        test = "Lorem" + " ممد " + " ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et commodo dui. Suspendisse justo metus, mmd nec auctor ut, pharetra a velit. Vivamus mammad quam," + stringToAppend + " dictum in tellus convallis, lobortis lacinia erat. Nulla elit eros, feugiat ac mammad cursus, fermentum eu lacus. Fusce dictum vestibulum libero, ac cursus erat condimentum at. Phasellus posuere egestas nisi, quis iaculis turpis interdum et. Fusce et blandit sem.\n" + stringToAppend +
                "\n" +
                "mammad imperdiet libero eu tellus elementum, a mammad tortor euismod. Proin fringilla arcu vitae dapibus blandit. Quisque interdum varius pellentesque. Pellentesque" + stringToAppend + "habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi pulvinar massa." + " " + stringToAppend + "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et commodo dui. Suspendisse justo metus, mmd nec auctor ut, pharetra a velit. Vivamus mammad quam," + stringToAppend + " dictum in tellus convallis, lobortis lacinia erat. Nulla elit eros, feugiat ac mammad cursus, fermentum eu lacus. Fusce dictum vestibulum libero, ac cursus erat condimentum at. Phasellus posuere egestas nisi, quis iaculis turpis interdum et. Fusce et blandit sem.\n" + stringToAppend +
                "\n" +
                "mammad imperdiet libero eu tellus elementum, a mammad tortor euismod. Proin fringilla arcu vitae dapibus blandit. Quisque interdum varius pellentesque. Pellentesque" + stringToAppend + "habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi pulvinar massa." + " " + stringToAppend + "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et commodo dui. Suspendisse justo metus, mmd nec auctor ut, pharetra a velit. Vivamus mammad quam," + stringToAppend + " dictum in tellus convallis, lobortis lacinia erat. Nulla elit eros, feugiat ac mammad cursus, fermentum eu lacus. Fusce dictum vestibulum libero, ac cursus erat condimentum at. Phasellus posuere egestas nisi, quis iaculis turpis interdum et. Fusce et blandit sem.\n" + stringToAppend +
                "\n" +
                "mammad imperdiet libero eu tellus elementum, " + "اصغر" + " a mammad tortor euismod. Proin fringilla arcu vitae dapibus blandit. Quisque interdum varius pellentesque. Pellentesque" + stringToAppend + "habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. " + "لورم اپسیوم" + " Morbi pulvinar massa." + " " + stringToAppend +
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et commodo dui. Suspendisse justo metus, mmd nec auctor ut, pharetra a velit. Vivamus mammad quam," + stringToAppend + " dictum in tellus convallis, lobortis lacinia erat. Nulla elit eros, feugiat ac mammad cursus, fermentum eu lacus. Fusce dictum vestibulum libero, ac cursus erat condimentum at. Phasellus posuere egestas nisi, quis iaculis turpis interdum et. Fusce et blandit sem.\n" + stringToAppend +
                "\n" +
                "mammad imperdiet libero eu tellus, a mammad tortor euismod. Proin fringilla arcu vitae dapibus blandit. Quisque interdum varius pellentesque. Pellentesque" + stringToAppend + "habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi pulvinar massa." + " " + stringToAppend;
        test += test;
        test += test;

        textView1.setText(test);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                findText(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


//        stringBuilder.append(stringToAppend);
//        stringBuilder.setSpan(new RoundedBackgroundSpan(this),
//                stringBuilder.length() - stringToAppend.length(),
//                stringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


//        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
//
//        String between = "";
//        for (String tag : eventListing.getTags()) {
//            stringBuilder.append(between);
//            if (between.length() == 0) between = "  ";
//            String thisTag = "  "+tag+"  ";
//            stringBuilder.append(thisTag);
//            stringBuilder.setSpan(new RoundedBackgroundSpan(this), stringBuilder.length() - thisTag.length(), stringBuilder.length() - thisTag.length() + thisTag.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//            //stringBuilder.setSpan(new BackgroundColorSpan(getResources().getColor(R.color.gray)), stringBuilder.length() - thisTag.length(), stringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        }
//
//        TextView tv = new TextView(this);
//        tv.setText(stringBuilder);

//        Toolbar toolbar = findViewById(R.id.toolbar);
////        setSupportActionBar(toolbar);
//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

    }

    private void findText(String toString) {
        RoundedBackgroundSpan.ys.clear();
        Pattern word = Pattern.compile(toString);
//        Pattern word2 = Pattern.compile(toString);
        Matcher match = word.matcher(test);
//        Matcher match2 = word2.matcher(test);
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(test);

        while (match.find()) {
            int start = match.start();
            int end = match.end();

            if (!toString.isEmpty()) {
                start = getCorrectStart(start);

                end = getCorrectEnd(end);
            }

            stringBuilder.setSpan(new RoundedBackgroundSpan(this, 0, 0),
                    start,
                    end,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

//        while (match2.find()) {
//            stringBuilder.setSpan(new RoundedBackgroundSpan(this, R.color.white, R.color.textfind),
//                    match2.start(),
//                    match2.end(),
//                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        }
        textView1.setText(stringBuilder);
        textView1.computeScroll();
    }

    private int getCorrectEnd(int end) {
        for (int i = end; i < test.length(); i++) {
            if (test.charAt(i) == ' ') {
                return i;
            }
        }
        return end;
    }

    private int getCorrectStart(int start) {
        for (int i = start; i > 0; i--) {
            if (test.charAt(i) == ' ') {
                return i + 1;
            }
        }
        return start;
    }

    public void gotoset(View view) {
        startActivity(new Intent(this, Main2Activity.class));
    }


}

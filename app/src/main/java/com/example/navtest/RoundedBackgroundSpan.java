package com.example.navtest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.style.ReplacementSpan;

import java.util.ArrayList;

public class RoundedBackgroundSpan extends ReplacementSpan {

    private static int CORNER_RADIUS = 5;
    private int backgroundColor = 0;
    private int textColor = 0;
    public static ArrayList<Integer> ys = new ArrayList<>();

    public RoundedBackgroundSpan(Context context, int textColor, int backgroundColor) {
        super();

        if (backgroundColor == 0) {
            this.backgroundColor = context.getResources().getColor(R.color.grey);
        }else {
            this.backgroundColor = context.getResources().getColor(backgroundColor);
        }
        if (textColor == 0) {
            this.textColor = context.getResources().getColor(R.color.white);
        } else {
            this.textColor = context.getResources().getColor(textColor);
        }
    }

    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
        RectF rect = new RectF(x - 3, top, x + measureText(paint, text, start, end) + 3, bottom);
        paint.setColor(backgroundColor);
        canvas.drawRoundRect(rect, CORNER_RADIUS, CORNER_RADIUS, paint);
        paint.setColor(textColor);
        add(y);
        canvas.drawText(text, start, end, x, y, paint);
    }

    private void add(int y) {
        for (Integer i : ys) {
            if (y == i) {
                return;
            }
        }
        ys.add(y);
    }

    @Override
    public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
        return Math.round(paint.measureText(text, start, end));
    }

    private float measureText(Paint paint, CharSequence text, int start, int end) {
        return paint.measureText(text, start, end);
    }
}